﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class NeedleKids_Hole : MonoBehaviour
{
    public GameObject test2;//実はいらないかも
    void OnTriggerEnter2D(Collider2D c)
    {
        // 相手のレイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);
        // 相手のレイヤー名がthreadの時は
        if (layerName == "thread")
        {
            Needle.judge = false;
            print("Holeで当たりました");
        }
    }
}
