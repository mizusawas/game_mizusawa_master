﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    //public Vector2 SPEED = new Vector2(0.05f, 0.05f);
    public float kisekispeed = 1f;
    public float kansei = 1f;
    public GameObject itokiseki;
    public Transform spawnPoint;
    bool isCollide = false;
    int passNum = 0;

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        Needle.judge = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isCollide == true)
        {
            Time.timeScale = 0;
        }
    }

	private void FixedUpdate()
	{
            Move();
            Kiseki();
    }


	void Kiseki()
    {
        GameObject newkiseki =
            Instantiate(itokiseki, spawnPoint.position, Quaternion.identity) as GameObject;
        newkiseki.GetComponent<Rigidbody2D>().velocity = transform.right * -1 * kisekispeed;
    }


    // 移動関数
    void Move()
    {
        // 現在位置をPositionに代入
        //Vector2 Position = transform.position;
        // 左キーを押し続けていたら
        if (Input.GetKey("up"))
        { // 上キーを押し続けていたら
          // 代入したPositionに対して加算減算を行う
            //Position.y += SPEED.y;
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * kansei);
        }
        else if (Input.GetKey("down"))
        { // 下キーを押し続けていたら
          // 代入したPositionに対して加算減算を行う
            //Position.y -= SPEED.y;
            GetComponent<Rigidbody2D>().AddForce(Vector2.down * kansei);
        }
        // 現在の位置に加算減算を行ったPositionを代入する
        //transform.position = Position;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        // プレイヤーを削除
        if (c.tag == "Enemy")
        {
            isCollide = true;
        }
        if(c.tag =="Hole")
        {
            passNum = passNum + 1;
            print(passNum);
        }


    }

    public bool getIsCollide()
    {
        return isCollide;
    }

    public int getPassNum()
    {
        return passNum;
    }
}