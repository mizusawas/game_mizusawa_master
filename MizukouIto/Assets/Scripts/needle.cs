﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;

public class Needle : MonoBehaviour {
    //針のスピード変数　unity側で値入れてるかなぁ
    public float speed;
    
    //針生成から削除するまでの時間　生存時間。
    public float lifeTime = 0.1f;
    //針を生成後、Destroyメソッドで消去したい針を指定するための変数
    public GameObject hari;
    //時間をカウントするフロート型変数
    float time = 0f;

    

    public static  bool  judge = false;　// judgeは、falseならゲーム続行　trueならゲーム中断（針の動きを止める）

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.right * -1 * speed;

    }

    void Update()
    {
        //deltaTimeは、Updateを１フレーム毎に何秒かかるかを返す。
        //Updateを１秒あたり130やるなら、値は1/130 １秒あたり60やるなら1/60 
        //timeは1フレーム事に経過時間を加えられる。
        //Unity側のログでtimeの状態を見られる。いらないならコメントアウト
        //print(time);
        //もしtimeが、lifeTimeより大きくなったら、生成したゲームオブジェクトをデストロイする。


        time += Time.deltaTime;
        if (time > lifeTime)
        {
            Destroy(hari);
        }

        
        if (judge == false)
        {
            //judgeがfalseなら、左に移動し続ける
            GetComponent<Rigidbody2D>().velocity = transform.right * -1 * speed;
        }
        else  
        {   //judgeがtrueなら、StopNeedleメソッドを使って停止させる。
            StopNeedle();
        }
        

    }

    //針を止めるメソッド
    void StopNeedle()
    {
        //速度を0にする
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        // print("[デバッグ用]Needle.csで止めたよ～");
        judge = false;

    }
    
    //当たり判定のメソッド　引数は、衝突相手のCollider2D
    /*
    void OnTriggerEnter2D(Collider2D c)
    {
        //print("[デバッグ用]あたったぞ");
        // 相手のレイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);
        // 相手のレイヤー名がthreadの時は
        if (layerName == "thread")
        {
       //     print("[デバッグ用]Needle.csでレイヤーで区別できたぞ");
            Needle.judge = true;
        }
       
    } 
    */
    /*オブジェクトのレイヤーの設定方法は、
     * ヒエラルキーからゲームオブジェクトを選択して、
     * インスペクターの上の方にある、Layerから選んで下さい。
     * 初期設定はDefault
     * SpriteRenderコンポーネントのSortingLayerは、Layerと違うので注意。
     * 
     * 勘違いで小一時間悩んだ
     */

}

