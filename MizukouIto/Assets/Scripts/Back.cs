﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Back : MonoBehaviour {
    public float lifeTime = 1.5f;
    float time = 0f;
	// Use this for initialization
	void Start () {
        time = 0;
	}
	
	// Update is called once per frame
	void Update () {
        //deltaTimeは、Updateを１フレーム毎に何秒かかるかを返す。
        //Updateを１秒あたり130やるなら、値は1/130 １秒あたり60やるなら1/60 
        //timeは1フレーム事に経過時間を加えられる。
        time += Time.deltaTime;
        //Unity側のログでtimeの状態を見られる。いらないならコメントアウト
        print(time);
        //もしtimeが、lifeTimeより大きくなったら、生成したゲームオブジェクトをデストロイする。
        if (time > lifeTime)
        {
            Destroy(gameObject);
        }
	}
}
