﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreCorrent : MonoBehaviour {
    public Text currentScore;
    public GameObject ito;
    PlayerScript script;
    int passNum = 0;

	// Use this for initialization
	void Start () {
        currentScore = this.gameObject.GetComponent<Text>();
        ito = GameObject.Find("ito 1");
        script = ito.GetComponent<PlayerScript>();
        currentScore.text = "現在" + passNum + "本目";
	}
	
	// Update is called once per frame
	void Update () {
		if(passNum != script.getPassNum()){
            passNum = script.getPassNum();
            currentScore.text = "現在" + passNum + "本目";
        }
	}
}
