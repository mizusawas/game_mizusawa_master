﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNeedle : MonoBehaviour {
    //配列型のゲームオブジェクト変数のneedlesを定義。作る針の個数と、針の指定をUnity側で行う。
    public GameObject[] needles;
    //ランダム生成した針を格納する変数
    public GameObject hari;
    // 配列needlesの添え字用変数
    public int number;
    public float time = 0;
    public float creatTime;
    



    void Start()
    {
        
    }


    void Update()
    {
        time += Time.deltaTime;
        if (time > creatTime)
        {
            //変数ナンバーは、0からneedlesの要素数までの値をとる。
            number = Random.Range(0, needles.Length);
            //変数hariに、インスタンスを格納。
            hari = Instantiate(needles[number]) as GameObject;
            hari.transform.position = new Vector2(Random.Range(7, 10), Random.Range(-3, 3));
            time = 0;
        }
    }
}
