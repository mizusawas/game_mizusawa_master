﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class resultTextScript : MonoBehaviour {
    Text resultText;
    GameObject ito;
    public PlayerScript script;
    string path;

	// Use this for initialization
	void Start () {
        path = Application.dataPath + @"\Resources\score.txt";
        ito = GameObject.Find("ito 1");
        script = ito.GetComponent<PlayerScript>();
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (script.getIsCollide()) {
            gameObject.SetActive(true);
            resultText.text = "成果:" + script.getPassNum() + "本";
            writeResult(script.getPassNum());
        }
	}

    void writeResult(int result) {
        File.WriteAllText(path, result.ToString());
    }
}
