﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class NeedleKids_Bottom : MonoBehaviour
{

    public GameObject test3;//実はいらないかも

    void OnTriggerEnter2D(Collider2D c)
    {
        // 相手のレイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);
        // レイヤー名がthreadの時は
        if (layerName == "thread")
        {
            Needle.judge = true;
        }
    }
}
