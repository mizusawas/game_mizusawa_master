﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedleKids_Top : MonoBehaviour
{

    public GameObject test1;//実はいらないかも

    void OnTriggerEnter2D(Collider2D c)
    {
        // 相手のレイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);
        // 相手のレイヤー名がthreadの時は
        if (layerName == "thread")
        {
            Needle.judge = true;
        }
    }
}
