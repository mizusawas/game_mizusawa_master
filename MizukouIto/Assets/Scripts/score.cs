﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour {
    Text scoreText;

	// Use this for initialization
	void Start () {
        scoreText = gameObject.GetComponent<Text>();
        TextAsset textasset = new TextAsset(); //テキストファイルのデータを取得するインスタンスを作成
        textasset = Resources.Load("score", typeof(TextAsset)) as TextAsset; //Resourcesフォルダから対象テキストを取得
        string textLines = textasset.text; //テキスト全体をstring型で入れる変数を用意して入れる
        scoreText.text += " " + textLines;

        //int scoreNum = int.Parse(textLines);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
